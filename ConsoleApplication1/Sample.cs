﻿using System;
namespace PatternRecognition
{
	/// <summary>
	/// Enum określający do której klasy należy dana próbka
	/// </summary>
	public enum TypeOfClass { First = 1, Second }

	/// <summary>
	/// Reprezentuje próbkę postaci: (wartość, klasa)
	/// </summary>
	public class Sample
	{
		public double Value { get; set; }
		public TypeOfClass Class { get; set; }
		public Sample() { }
		public Sample(Sample sampleToCopy)
		{
			this.Value = sampleToCopy.Value;
			this.Class = sampleToCopy.Class;
		}
		public override string ToString()
		{
			return "Value: " + Value.ToString() + "\tClass: " + Class.ToString();
		}
	}
}
