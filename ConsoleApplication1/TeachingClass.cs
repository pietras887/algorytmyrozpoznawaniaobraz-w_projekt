﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternRecognition
{
	/// <summary>
	/// Reprezentuje klasę uczącą na podstawie zadanego ciągu uczącego
	/// </summary>
	public class TeachingClass
	{
		List<Sample> teachingSamples;

		/// <summary>
		/// Inicjuje instancję obiektu
		/// </summary>
		/// <param name="teachingSamples">zestaw danych uczących</param>
		public TeachingClass(List<Sample> teachingSamples)
		{
			this.teachingSamples = teachingSamples;
		}

		/// <summary>
		/// Uczenie typu najbliższa średnia
		/// </summary>
		/// <param name="listOfSamples">zestaw danych do nauczenia</param>
		/// <returns></returns>
		public List<Sample> NearestMeanLearning(List<Sample> listOfSamples)
		{
			List<Sample> outputList = listOfSamples.ConvertAll((x) => new Sample(x));

			double meanClass1 = (from x in teachingSamples
								 where x.Class == TypeOfClass.First
								 select x.Value).Average();

			double meanClass2 = (from x in teachingSamples
								 where x.Class == TypeOfClass.Second
								 select x.Value).Average();

			foreach (var item in outputList)
			{
				if (Math.Abs(item.Value - meanClass1) < Math.Abs(item.Value - meanClass2))
					item.Class = TypeOfClass.First;
				else
					item.Class = TypeOfClass.Second;
			}
			return outputList;
		}

		/// <summary>
		/// Uczenie typu alfa-najbliższych sąsiadów
		/// </summary>
		/// <param name="listOfSamples">zestaw danych do nauczenia</param>
		/// <param name="numberOfNeighbours">liczba sąsiadów</param>
		/// <returns></returns>
		public List<Sample> NearestNeighbourLearning(List<Sample> listOfSamples, int numberOfNeighbours)
		{
			List<Sample> outputList = listOfSamples.ConvertAll((x) => new Sample(x));

			for (int i = 0; i < listOfSamples.Count; i++)
			{
				var sample = outputList[i];
				var neighbours = new List<Tuple<double, Sample>>();
				for (int j = 0; j < teachingSamples.Count; j++)
				{
					var tmp = teachingSamples[j];
					double distance = Math.Abs(sample.Value - tmp.Value);

					if (neighbours.Count < numberOfNeighbours)
						neighbours.Add(new Tuple<double, Sample>(distance, tmp));

					else if (distance < neighbours[numberOfNeighbours - 1].Item1)
					{
						neighbours.RemoveAt(numberOfNeighbours - 1);
						neighbours.Add(new Tuple<double, Sample>(distance, tmp));
					}

					neighbours.Sort((x, y) => x.Item1.CompareTo(y.Item1));
				}
				int countClass1 = (from x in neighbours
								   where x.Item2.Class == TypeOfClass.First
								   select x).ToList().Count;
				if (countClass1 > numberOfNeighbours / 2)
					sample.Class = TypeOfClass.First;
				else
					sample.Class = TypeOfClass.Second;
			}

			return outputList;
		}
	}
}
