﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternRecognition
{
	public enum Teaching { NEAREST_MEAN, N_NEAREST_NEIGHBOURS }
	public enum Distribution { UNIFORM, NORMAL }

	public class Parameters
	{
		public Teaching TeachingMethod { get; set; }
		public Distribution DistributionType { get; set; }
		public double[] DistributionParameters { get; set; }
		public int NumberOfTeachingSamples { get; set; }
		public int NumberOfSamples { get; set; }
		public int NumberOfIncorectlyClassified { get; set; }
		public int NumberOfNeighbours { get; set; }
		public double ProbabilityClassFirst { get; set; }
		public double ProbabilityClassSecond { get; set; }
	}
}
