﻿using System;
using System.Linq;
using System.Collections.Generic;
using Troschuetz.Random.Distributions.Continuous;


namespace PatternRecognition
{
	public class Program
	{
		static void Main(string[] args)
		{
			var parameters = new double[] { 0, 1, 5, 1 };
			int n = 100;
			double p = 0.5;
			int numberOfNeighbours = 1;
			//var v = myTeachingClass.NearestNeighbourLearning(testList, 3);
			/*Console.WriteLine("Before learning:");
			testList.ForEach((x) => Console.WriteLine(x));

			Console.WriteLine("\nAfter learning:");
			v.ForEach((x) => Console.WriteLine(x));*/
			using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"D:\" + numberOfNeighbours + "NM" + "_" + p + ".txt"))
			{
				for (int N = 10; N <= 10000; N *= 10)
				{
					int k = 0;
					const int repeats = 100;
					for (int j = 0; j < repeats; j++)
					{
						var teachingList = Generator.NormalDistribution(N, p, parameters);
						TeachingClass myTeachingClass = new TeachingClass(teachingList);
						var testList = Generator.NormalDistribution(n, p, parameters);
						var v = myTeachingClass.NearestMeanLearning(testList);
						//var v = myTeachingClass.NearestNeighbourLearning(testList, numberOfNeighbours);
						k += Math.Abs(v.Count((x) => x.Class == TypeOfClass.First) - testList.Count((x) => x.Class == TypeOfClass.First));
					}
					file.WriteLine(N + " " + k/repeats);
				}
			}


			/*Parameters param = new Parameters();
			param.TeachingMethod = Teaching.N_NEAREST_NEIGHBOURS;
			param.DistributionType = Distribution.NORMAL;
			param.DistributionParameters = parameters;
			param.NumberOfTeachingSamples = N;
			param.NumberOfSamples = n;
			param.NumberOfIncorectlyClassified = k;
			param.ProbabilityClassFirst = p;
			param.ProbabilityClassSecond = 1.0 - p;
			param.NumberOfNeighbours = numberOfNeighbours;

			ExcelWriter.ExportGenericListToExcel(param, testList, v, @"D:\plik_" + N + "_" + p + "_"+param.TeachingMethod+numberOfNeighbours.ToString()+".xls");
			//Console.ReadKey();*/
		}
	}
}
