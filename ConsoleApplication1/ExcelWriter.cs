﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternRecognition
{
	public static class ExcelWriter
	{
		public static void ExportGenericListToExcel<T>(Parameters parameters, List<T> beforeTeaching, List<T> afterTeaching, string excelFilePath)
		{
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

			SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
			ExcelFile file = new ExcelFile();
			ExcelWorksheet sheet = file.Worksheets.Add("Exported List");
			int n = properties.Count;

			SaveParameters(sheet, parameters);
			sheet.Cells[12, 0].Value = "Przed uczeniem";
			sheet.Cells[12, 2].Value = "Po uczeniu";

			for (int i = 0; i < n; i++)
			{
				sheet.Cells[13, i].Value = properties[i].Name;
				sheet.Cells[13, i + n].Value = properties[i].Name;
			}

			int p = 14;
			for (int i = 0; i < beforeTeaching.Count; i++)
				for (int j = 0; j < properties.Count; j++)
				{
					sheet.Cells[i + p, j].Value = properties[j].GetValue(beforeTeaching[i]);
					sheet.Cells[i + p, j + n].Value = properties[j].GetValue(afterTeaching[i]);
				}

			file.Save(excelFilePath);
		}

		public static void SaveParameters(ExcelWorksheet sheet, Parameters parameters)
		{
			string[] label;

			if (parameters.DistributionType == Distribution.UNIFORM)
				label = new string[] { "a1", "b1", "a2", "b2" };
			else
				label = new string[] { "mu1", "variation1", "mu2", "variation2" };

			sheet.Cells[0, 0].Value = parameters.TeachingMethod.ToString();
			sheet.Cells[1, 0].Value = parameters.DistributionType.ToString();

			for (int i = 0; i < 4; i++)
			{
				sheet.Cells[i + 2, 0].Value = label[i];
				sheet.Cells[i + 2, 1].Value = parameters.DistributionParameters[i];
			}
			sheet.Cells[6, 0].Value = "Dlugosc ciagu uczacego";
			sheet.Cells[6, 1].Value = parameters.NumberOfTeachingSamples;

			sheet.Cells[7, 0].Value = "Dlugosc ciagu do uczenia";
			sheet.Cells[7, 1].Value = parameters.NumberOfSamples;

			sheet.Cells[8, 0].Value = "Liczba niepoprawnie zaklasyfikowanych";
			sheet.Cells[8, 1].Value = parameters.NumberOfIncorectlyClassified;

			sheet.Cells[9, 0].Value = "Prawdopodobienstwo klasy pierwszej";
			sheet.Cells[9, 1].Value = parameters.ProbabilityClassFirst;
			sheet.Cells[10, 0].Value = "Prawdopodobienstwo klasy drugiej";
			sheet.Cells[10, 1].Value = parameters.ProbabilityClassSecond;

			if (parameters.TeachingMethod == Teaching.N_NEAREST_NEIGHBOURS)
			{
				sheet.Cells[11, 0].Value = "Liczba sasiadow";
				sheet.Cells[11, 1].Value = parameters.NumberOfNeighbours;
			}

		}
	}
}
