﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Troschuetz.Random.Distributions.Continuous;

namespace PatternRecognition
{
	public static class RandomExtension
	{
		public static double NextGausian(this Random r, int seed, double mu = 0, double variation = 1)
		{
			var number = new NormalDistribution((uint)Guid.NewGuid().GetHashCode(), mu, variation);
			return number.NextDouble();
		}
	}
}
