﻿using System;
using System.Collections.Generic;
using Troschuetz.Random.Distributions.Continuous;

namespace PatternRecognition
{
	/// <summary>
	/// Statyczna klasa reprezentująca generator ciągów próbek
	/// </summary>
	public static class Generator
	{
		/// <summary>
		/// Generuje ciąg uczący o razkładzie jednostajnym
		/// </summary>
		/// <param name="n">Długość ciągu uczącego</param>
		/// <param name="probability">Prawdopodobieństwo dla klasy First</param>
		/// <param name="parameters">parametry dla generatora: {a1, b1, a2, b2}</param>
		/// <exception cref="System.ArgumentException"></exception>
		/// <returns></returns>
		public static List<Sample> UniformDistribution(int n, double probability, double[] parameters)
		{
			
			if (parameters.Length != 4)
				throw new ArgumentException("Not enough parameters");

			var teachingSequence = new List<Sample>();
			Random rand = new Random(Guid.NewGuid().GetHashCode());
			double d1 = parameters[1] - parameters[0];
			double d2 = parameters[3] - parameters[2];

			for (int i = 0; i < n; i++)
			{
				if (rand.NextDouble() < probability)
				{
					Sample s = new Sample();
					s.Value = parameters[0] + rand.NextDouble() * d1;
					s.Class = TypeOfClass.First;
					teachingSequence.Add(s);
				}
				else
				{
					Sample s = new Sample();
					s.Value = parameters[2] + rand.NextDouble() * d2;
					s.Class = TypeOfClass.Second;
					teachingSequence.Add(s);
				}
			}

			return teachingSequence;
		}
		/// <summary>
		/// Generuje ciąg uczący o rozkładzie normalnym
		/// </summary>
		/// <param name="n">długość ciągu uczącego</param>
		/// <param name="probability">prawdopodobieństwo dla klasy First</param>
		/// <param name="parameters">parametry dla generatora: {mu1, variation1, mu2, variation2}</param>
		/// <exception cref="System.ArgumentException"></exception>
		/// <returns></returns>
		public static List<Sample> NormalDistribution(int n, double probability, double[] parameters)
		{
			if (parameters.Length != 4)
				throw new ArgumentException("Not enough parameters");

			var teachingSequence = new List<Sample>();

			Random rand = new Random(Guid.NewGuid().GetHashCode());
			for (int i = 0; i < n; i++)
			{
				var seed = Guid.NewGuid().GetHashCode();
				if (rand.NextDouble() < probability)
				{
					Sample s = new Sample();
					s.Value = rand.NextGausian(seed, parameters[0], parameters[1]);
					s.Class = TypeOfClass.First;
					teachingSequence.Add(s);
				}
				else
				{
					Sample s = new Sample();
					s.Value = rand.NextGausian(seed, parameters[2], parameters[3]);
					s.Class = TypeOfClass.Second;
					teachingSequence.Add(s);
				}
			}
			return teachingSequence;
		}
	}
}
